/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entidad;

/**
 *
 * @author ANDERSSON
 */
public class Empleado {
    int codigo;
    String nombre;
    String apellido;
    String dni;
    String Direccion;
    String telefono;
    int estado;

    public Empleado(int codigo, String nombre, String apellido, String dni, String Direccion, String telefono, int estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.Direccion = Direccion;
        this.telefono = telefono;
        this.estado = estado;
    }

    public Empleado(String nombre, String apellido, String dni, String Direccion, String telefono, int estado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.Direccion = Direccion;
        this.telefono = telefono;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    
}
